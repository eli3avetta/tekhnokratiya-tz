import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './main-layout/main-layout.component';

let routes: Routes;
routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'users-list/:status',
        loadChildren: () => import('./pages/users-list/users-list.module')
          .then(m => m.UsersListModule)
      },
      {
        path: '',
        redirectTo: 'users-list/all'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
