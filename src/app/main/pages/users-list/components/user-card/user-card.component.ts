import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { UserSearchResponse } from '../../../../../common/interfaces/user-search-response';
import { checkExistParent } from '../../../../../common/utils/check-exist-parent';
import { UserInfoModalComponent } from '../../../../../common/modals/user-info-modal/user-info-modal.component';
import { SUBSCRIPTION_STATUSES } from '../../../../../common/utils/subscription-statuses';
import { SelectComponent } from '../../../../../common/components/controls/select/select.component';
import { ModalService } from '../../../../../common/services/modal/modal.service';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
  @Input() user: UserSearchResponse;
  @ViewChild(SelectComponent, { static: false, read: ElementRef }) private readonly _selectComponent: ElementRef;

  readonly subscriptionStatuses = SUBSCRIPTION_STATUSES;

  constructor(private readonly _modalService: ModalService) {
  }

  ngOnInit(): void {
  }

  openModal(event: MouseEvent, user: UserSearchResponse): void {
    if (!checkExistParent(event.target, this._selectComponent.nativeElement)) {
      this._modalService.open<UserSearchResponse>(UserInfoModalComponent, {
        data: user
      });
    }
  }
}
