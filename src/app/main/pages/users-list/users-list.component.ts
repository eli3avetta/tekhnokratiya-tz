import { ChangeDetectionStrategy, Component, HostListener, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { UserSearchResponse } from '../../../common/interfaces/user-search-response';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { UsersListParams } from '../../../common/enums/users-list-params';
import { catchError, finalize, map, switchMap, tap } from 'rxjs/operators';
import { UsersApiService } from '../../../common/services/api/users-api.service';

@UntilDestroy()
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersListComponent implements OnInit {
  private readonly _usersList$ = new BehaviorSubject<UserSearchResponse[] | null>([]);
  private readonly _isBottomPage$ = new BehaviorSubject<boolean>(false);
  private readonly _isLoading$ = new BehaviorSubject<boolean>(false);
  private readonly _isError$ = new BehaviorSubject<boolean>(false);

  readonly usersList$ = this._usersList$.asObservable();
  readonly isBottomPage$ = this._isBottomPage$.asObservable();
  readonly isLoading$ = this._isLoading$.asObservable();

  constructor(private readonly _activatedRoute: ActivatedRoute,
              private readonly _usersApiService: UsersApiService) {
  }

  @HostListener('window:scroll') scroll(): void {
    if (window.scrollY + 1 >= document.documentElement.scrollHeight - document.documentElement.clientHeight) {
      this._isBottomPage$.next(true);
    } else {
      this._isBottomPage$.next(false);
    }
  }

  ngOnInit(): void {
    const pageData: UserSearchResponse[] = this._activatedRoute.snapshot.data.pageData;
    if (pageData) {
      this._usersList$.next(pageData);
    } else {
      this._isError$.next(true);
    }

    this._activatedRoute.params
      .pipe(
        untilDestroyed(this),
        tap(() => this._isLoading$.next(true)),
        switchMap(param => {
          return this._usersApiService.searchUsers()
            .pipe(
              untilDestroyed(this),
              finalize(() => this._isLoading$.next(false)),
              map(usersList => {
                switch (true) {
                  case param.status === UsersListParams.Blocked:
                    return usersList.filter(user => user.status === 2);

                  case param.status === UsersListParams.Active:
                    return usersList.filter(user => user.status === 1);

                  default:
                    return usersList;
                }
              }),
              catchError(() => of(null))
            );
        })
      )
      .subscribe(userList => {
          this._usersList$.next(userList);
        },
        (error) => {
          this._isError$.next(true);
          console.error(error);
        });
  }
}
