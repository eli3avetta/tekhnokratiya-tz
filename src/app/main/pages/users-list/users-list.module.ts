import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { UsersListComponent } from './users-list.component';
import { RouterModule, Routes } from '@angular/router';
import { UsersListResolve } from './users-list.resolve';
import { PipesModule } from '../../../common/pipes/pipes.module';
import { SelectModule } from '../../../common/components/controls/select/select.module';
import { ClickOutsideModule } from '../../../common/directives/click-outside/click-outside.module';
import { UserCardComponent } from './components/user-card/user-card.component';

const routes: Routes = [
  {
    path: '',
    component: UsersListComponent,
    resolve: { pageData: UsersListResolve }
  }
];

@NgModule({
  declarations: [
    UsersListComponent,
    UserCardComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PipesModule,
    SelectModule,
    ClickOutsideModule,
  ],
  providers: [
    UsersListResolve,
    DatePipe
  ]
})
export class UsersListModule {
}
