import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { UserSearchResponse } from '../../../common/interfaces/user-search-response';
import { UsersApiService } from '../../../common/services/api/users-api.service';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { UsersListParams } from '../../../common/enums/users-list-params';

@Injectable()
export class UsersListResolve implements Resolve<UserSearchResponse[] | null> {
  constructor(private readonly _usersApiService: UsersApiService,
              private readonly _activatedRoute: ActivatedRoute) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<UserSearchResponse[] | null> {
    const statusList = route.params.status;
    return this._usersApiService.searchUsers()
      .pipe(
        map(usersList => {
          switch (true) {
            case statusList === UsersListParams.Blocked:
              return usersList.filter(user => user.status === 2);

            case statusList === UsersListParams.Active:
              return usersList.filter(user => user.status === 1);

            default:
              return usersList;
          }
        }),
        catchError(() => of(null))
      );
  }
}
