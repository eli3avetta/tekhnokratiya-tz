import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-auth-layout',
  templateUrl: './not-auth-layout.component.html',
  styleUrls: ['./not-auth-layout.component.scss']
})

export class NotAuthLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
