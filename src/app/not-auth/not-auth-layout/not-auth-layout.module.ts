import { NgModule } from '@angular/core';
import { NotAuthLayoutComponent } from './not-auth-layout.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    NotAuthLayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class NotAuthLayoutModule {
}
