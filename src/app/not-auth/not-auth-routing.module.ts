import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotAuthLayoutComponent } from './not-auth-layout/not-auth-layout.component';

const routes: Routes = [
  {
    path: '',
    component: NotAuthLayoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotAuthRoutingModule {
}
