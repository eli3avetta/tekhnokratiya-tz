const RULES = {
  // 1 | 2 | 5
  ['час']: ['час', 'часа', 'часов'],
  ['минуту']: ['минуту', 'минуты', 'минут'],
  ['секунду']: ['секунду', 'секунды', 'секунд']
};

export function pluralize(word: string, count: number | string): string {
  const titles = RULES[word];
  const cases = [2, 0, 1, 1, 1, 2];
  count = Math.abs(+count);
  if (count === 0) {
    return titles[2];
  } else if (Math.round(count) === +count) {
    if (+count < 1) {
      return titles[1];
    }
    count = Math.round(Math.abs(count));

    return titles[(count % 100 > 4 && count % 100 < 20) ? 2 : cases[(count % 10 < 5) ? count % 10 : 5]];
  } else {
    return titles[1];
  }
}
