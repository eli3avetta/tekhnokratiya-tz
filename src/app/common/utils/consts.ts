export const DAY_IN_MS = 86400000;
export const HOUR_IN_MS = 3600000;
export const MIN_IN_MS = 60000;
