import { Component, HostListener, Input, OnInit } from '@angular/core';
import { ModalService } from '../services/modal/modal.service';
import { Subject } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { withLatestFrom } from 'rxjs/operators';
import { Modal } from '../services/modal/modal';

@UntilDestroy()
@Component({ template: '' })
export abstract class ModalBase<TDataIn = unknown> implements OnInit {
  protected readonly _close$ = new Subject();

  @Input() protected readonly _modal: Modal<unknown, TDataIn>;
  data: TDataIn;

  protected constructor(protected readonly _modalService: ModalService) {
  }

  @HostListener('window:click', ['$event']) windowClick(event: MouseEvent): void {
    const target: Element = event.target as Element;

    switch (true) {
      case target.classList.contains('gl-modal-btn-close'):
      case target.classList.contains('gl-main-btn'):
        this.close();
        break;

      case target.classList.contains('gl-modal-wrapper') && this._modal?.options?.closeByOut:
        this.close();
    }
  }

  @HostListener('window:keydown.esc') windowKeydownEsc(): void {
    if (this._modal?.options?.closeByEsc) {
      this.close();
    }
  }

  ngOnInit(): void {
    this.data = this._modal.options?.data;

    this._close$
      .pipe(
        withLatestFrom(this._modalService.openedModals$),
        untilDestroyed(this)
      )
      .subscribe(([, openedModals]) => {
        if (openedModals[openedModals.length - 1].id === this._modal.id) {
          this._modalService.close();
        }
      });
  }

  close(): void {
    this._close$.next();
  }
}
