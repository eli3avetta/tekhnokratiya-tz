export interface ModalOptions<TData = unknown> {
  closeByOut?: boolean;
  closeByEsc?: boolean;
  data?: TData;
}
