import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserInfoModalComponent } from './user-info-modal.component';

@NgModule({
  declarations: [
    UserInfoModalComponent
  ],
  imports: [
    CommonModule
  ],
  entryComponents: [
    UserInfoModalComponent
  ]
})
export class UserInfoModalModule {
}
