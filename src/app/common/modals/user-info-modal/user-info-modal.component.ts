import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../services/modal/modal.service';
import { ModalBase } from '../../abstractions/modal-base';
import { UserSearchResponse } from '../../interfaces/user-search-response';
import { SUBSCRIPTION_STATUSES } from '../../utils/subscription-statuses';

@Component({
  selector: 'app-user-info-modal',
  templateUrl: './user-info-modal.component.html',
  styleUrls: ['./user-info-modal.component.scss']
})
export class UserInfoModalComponent extends ModalBase<UserSearchResponse> implements OnInit {
  readonly subscriptionStatuses = SUBSCRIPTION_STATUSES;

  constructor(modalService: ModalService) {
    super(modalService);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }
}
