import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserSearchResponse } from '../../interfaces/user-search-response';

@Injectable({ providedIn: 'root' })
export class UsersApiService {
  constructor(private http: HttpClient) {
  }

  searchUsers(): Observable<UserSearchResponse[]> {
    return this.http.get<any>('https://watchlater.cloud.technokratos.com/get/array');
  }
}
