import { Injectable, Renderer2, RendererFactory2, Type } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Modal } from './modal';
import { ModalOptions } from '../../interfaces/modal-options';

@Injectable({ providedIn: 'root' })
export class ModalService {
  private incId = 1;
  private readonly _renderer: Renderer2;
  private readonly _openedModals$ = new BehaviorSubject<Modal[]>([]);
  private readonly _open$ = new Subject<Modal>();
  private readonly _close$ = new Subject();

  readonly openedModals$ = this._openedModals$.asObservable();
  readonly open$ = this._open$.asObservable();
  readonly close$ = this._close$.asObservable();

  constructor(rendererFactory: RendererFactory2) {
    this._renderer = rendererFactory.createRenderer(null, null);

    this._openedModals$
      .subscribe(modals => {
        if (modals.length) {
          this._renderer.setStyle(document.body, 'overflow', 'hidden');
        } else {
          this._renderer.setStyle(document.body, 'overflow', 'initial');
        }
      });
  }

  open<TDataIn = unknown>(component: Type<unknown>, options?: ModalOptions<TDataIn>): void {
    const openedModals = [...this._openedModals$.value];
    const newModal = new Modal(this.incId, component, getDefaultOptions(options));
    openedModals.push(newModal);
    this._openedModals$.next(openedModals);
    this._open$.next(newModal);
    this.incId++;
  }

  close(): void {
    const openedModals = [...this._openedModals$.value];
    openedModals.pop();
    this._openedModals$.next(openedModals);
    this._close$.next();
  }
}

function getDefaultOptions(options?: ModalOptions): ModalOptions {
  return {
    closeByEsc: options?.closeByEsc ?? true,
    closeByOut: options?.closeByOut ?? true,
    data: options?.data
  };
}
