import { Type } from '@angular/core';
import { ModalOptions } from '../../interfaces/modal-options';

export class Modal<TComponent = unknown, TData = unknown> {
  readonly id: number;
  readonly component: Type<TComponent>;
  readonly options: ModalOptions<TData>;

  constructor(id: number,
              component: Type<TComponent>,
              options?: ModalOptions<TData>) {
    this.id = id;
    this.component = component;
    this.options = options;
  }
}
