import { Pipe, PipeTransform } from '@angular/core';
import { pluralize } from '../utils/pluralize.utils';
import { DAY_IN_MS, HOUR_IN_MS, MIN_IN_MS } from '../utils/consts';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'dateOfTimeElapsed'
})
export class DateOfTimeElapsedPipe implements PipeTransform {
  constructor(private readonly _datePipe: DatePipe) {
  }
  transform(value: string): string | null {
    if (value) {
      const currentDate = new Date();
      const lastUpdated = new Date(value);

      const diffMs = currentDate.getTime() - lastUpdated.getTime();
      const diffInSeconds = currentDate.getSeconds() - lastUpdated.getSeconds();

      switch (true) {
        case diffMs >= DAY_IN_MS:
          return this._datePipe.transform(lastUpdated, 'dd.MM.yyyy');

        case diffMs >= HOUR_IN_MS:
          const diffInHours = currentDate.getHours() - lastUpdated.getHours();
          return `${ diffInHours } ${ pluralize('час', diffInHours) } назад`;

        case diffMs >= MIN_IN_MS:
          const diffInMinutes = currentDate.getMinutes() - lastUpdated.getMinutes();
          return `${ diffInMinutes } ${ pluralize('минуту', diffInMinutes) } назад`;

        default:
          return `${ diffInSeconds } ${ pluralize('секунду', diffInSeconds) } назад`;
      }
    } else {
      return null;
    }
  }
}
