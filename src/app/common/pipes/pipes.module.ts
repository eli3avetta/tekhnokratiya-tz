import { NgModule } from '@angular/core';
import { ReduceNumberPipe } from './reduce-number.pipe';
import { DateOfTimeElapsedPipe } from './date-of-time-elapsed.pipe';

@NgModule({
  declarations: [
    ReduceNumberPipe,
    DateOfTimeElapsedPipe
  ],
  exports: [
    ReduceNumberPipe,
    DateOfTimeElapsedPipe
  ]
})
export class PipesModule {
}
