import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reduceNumber'
})
export class ReduceNumberPipe implements PipeTransform {
  transform(value: number, lengthDecimalPlaces): number {
    if (value) {
      return +value.toFixed(lengthDecimalPlaces);
    } else {
      return 0;
    }
  }
}
