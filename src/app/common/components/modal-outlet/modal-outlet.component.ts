import {
  ChangeDetectionStrategy,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { ModalService } from '../../services/modal/modal.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-modal-outlet',
  templateUrl: './modal-outlet.component.html',
  styleUrls: ['./modal-outlet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalOutletComponent implements OnInit {
  @ViewChild('modal', { static: true, read: ViewContainerRef }) private readonly _modalTemplate: ViewContainerRef;

  constructor(private readonly _modalService: ModalService,
              private readonly _componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {
    this._modalService.open$
      .pipe(untilDestroyed(this))
      .subscribe(modal => {
        const componentFactory = this._componentFactoryResolver.resolveComponentFactory(modal.component);
        const componentRef: ComponentRef<any> = this._modalTemplate.createComponent(componentFactory);
        componentRef.instance._modal = modal;
      });

    this._modalService.close$
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this._modalTemplate.remove(this._modalTemplate.length - 1);
      });
  }
}
