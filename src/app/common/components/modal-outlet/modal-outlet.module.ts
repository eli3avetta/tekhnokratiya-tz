import { NgModule } from '@angular/core';
import { ModalOutletComponent } from './modal-outlet.component';

@NgModule({
  declarations: [
    ModalOutletComponent
  ],
  exports: [
    ModalOutletComponent
  ]
})
export class ModalOutletModule {
}
