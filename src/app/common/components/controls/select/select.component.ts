import { Component, Input } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements ControlValueAccessor {
  private readonly _isOpen$ = new BehaviorSubject<boolean>(false);

  private _value = null;
  private _disabled = false;

  @Input() indexSelected: number;
  @Input() options: string[];
  readonly isOpen$ = this._isOpen$.asObservable();

  private onChange = (value: any) => {
  };
  private onTouched = () => {
  };

  constructor() {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(outsideValue: number): void {
    this._value = outsideValue;
  }

  setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }

  updateValue(insideValue: number): void {
    this._value = insideValue;
    this.onChange(insideValue);
  }

  toggleOpened(): void {
    this._isOpen$.next(!this._isOpen$.value);
  }


  close(): void {
    this._isOpen$.next(false);
  }
}
