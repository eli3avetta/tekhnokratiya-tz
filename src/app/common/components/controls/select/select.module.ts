import { NgModule } from '@angular/core';
import { SelectComponent } from './select.component';
import { CommonModule } from '@angular/common';
import { ClickOutsideModule } from '../../../directives/click-outside/click-outside.module';

@NgModule({
  declarations: [
    SelectComponent
  ],
  imports: [
    CommonModule,
    ClickOutsideModule
  ],
  exports: [
    SelectComponent
  ]
})
export class SelectModule {
}
