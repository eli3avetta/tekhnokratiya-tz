import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ModalOutletModule } from './common/components/modal-outlet/modal-outlet.module';
import { UserInfoModalModule } from './common/modals/user-info-modal/user-info-modal.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ModalOutletModule,

    // модалки
    UserInfoModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
